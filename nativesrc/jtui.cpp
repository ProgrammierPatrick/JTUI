#include <jni.h>
#include <ncurses.h>
#include "jtui_NCurses.h"

#define JNIFUNC(RETURN, NAME) JNIEXPORT RETURN JNICALL \
    Java_jtui_NCurses_jtui_1##NAME (JNIEnv *env, jclass cls) 
#define JNIFUNCARG(RETURN, NAME, ...) JNIEXPORT RETURN JNICALL \
    Java_jtui_NCurses_jtui_1##NAME (JNIEnv *env, jclass cls, __VA_ARGS__) 

//INIT
JNIFUNC(jlong, init) {
	WINDOW *scr = initscr(); //create ncurses

	cbreak(); //do not get [Ctrl]+[C] etc.
	noecho(); //typed characters are not visible
	keypad(stdscr, TRUE); //enable special keys (arrows, F1, etc.)
	
	//TODO: bestimmt falsch
	return (jlong) scr;
}

//CLOSE
JNIFUNC(void, close) {
	endwin();
}

//PRINTW
JNIFUNCARG(void, wprintw, jlong win, jstring str) {
//JNIEXPORT void JNICALL Java_jtui_NCurses_jtui_1wprintw(JNIEnv *env, jclass ths, jlong win,jstring str) {
	wprintw((WINDOW*)win, env->GetStringUTFChars(str, NULL));
	//refresh();
}

//REFRESH
JNIFUNCARG(void, wrefresh, jlong win) {
	wrefresh((WINDOW*) win);
}

//NEWWIN
JNIFUNCARG(jlong, newwin, jint height, jint width, jint starty, jint startx) {
	WINDOW *win = newwin(height, width, starty, startx);
	box(win, 0, 0);
	return (jlong) win;
}

//DELWIN
JNIFUNCARG(void, delwin, jlong win) {
	delwin((WINDOW*) win);
}

//GETMAXYX
JNIFUNCARG(jint, getmaxx, jlong win) {
	int y,x;
	getmaxyx((WINDOW*) win, y, x);
	return x;
}
JNIFUNCARG(jint, getmaxy, jlong win) {
	int y,x;
	getmaxyx((WINDOW*) win, y, x);
	return y;
}

//WMOVE
JNIFUNCARG(void, wmove, jlong win, int y, int x) {
	wmove((WINDOW*) win, y, x);
}

//GETCH
JNIFUNCARG(jchar, getch, jlong win) {
	return (jchar) wgetch((WINDOW*) win);
}
