rm -r bin/
mkdir bin/

# compile java classes
javac -sourcepath "src/" -d "bin/" src/jtui/*.java src/cz/adamh/utils/*.java

# create JNI header
javah -d "bin/" -cp "src/" jtui.NCurses

javainc=/usr/lib/jvm/default/include
g++ -I$javainc -I$javainc/linux/ -Ibin/ -shared -o bin/libjtui.so nativesrc/jtui.cpp -lncurses -fPIC

java -Djava.library.path=bin -cp bin jtui.JTUIDemo
