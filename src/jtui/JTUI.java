package jtui;


import java.io.IOException;
import java.io.Writer;
import java.io.PrintWriter;

public class JTUI {
	public static void main(String[] args) {
		JTUI.start();

		JTUI.out.println("Hello world.");
		JTUI.out.println();
		JTUI.out.print("X\n");
		
		JTUIWindow w = new JTUIWindow(4, 4, 5, 5);
		w.out.println("XXX");

		w.close();


		JTUI.end();
 	}

	private static JTUIWindow rootWindow;


	/** the Output stream to the Standard Screen for convenience. */
	public static PrintWriter out = null;

	/** Enters curses-mode. From now on System.in and System.out should not be used any more. */
	public static void start() {
		rootWindow = new JTUIWindow(NCurses.init());
		out = rootWindow.out;
	}

	/** Exits curses-mode. System.in and System.out may be used again. */
	public static void end() {
		rootWindow = null;
		NCurses.end();
	}

	/** returns Window of whole drawable surface. Maps to stdscr in ncurses. */
	public static JTUIWindow getRootWindow() {
		if(rootWindow == null) throw new RuntimeException("Cannot return RootWindow: JTUI is not initialized.");
		return rootWindow;
	}

}

