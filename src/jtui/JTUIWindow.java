package jtui;

import java.io.Writer;
import java.io.PrintWriter;


/** This class wraps a ncurses window.
 *  This is a rectangular Area on the screen in which can be written.
 *  It also features a PrintWriter that can be used for simple Writing like this:
 * 
 * JTUIWindow w = new JTUIWindow(0,0,10,10);
 * .out.println("Hello World!");
 */
public class JTUIWindow {
	private int x=0, y=0, width, height;
	private long windowPtr;

	/** A PrintWriter for output to this Window. */
	public PrintWriter out;

	/** Construct a Window from coordinates. Keep sure that JTUI.start() has already be called */
	public JTUIWindow(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		windowPtr = NCurses.newWin(x, y, width, height);
		out = new PrintWriter(new JTUIWriter(windowPtr));
	}

	JTUIWindow(long ptr) {
		windowPtr = ptr;
		out = new PrintWriter(new JTUIWriter(windowPtr));
		width = NCurses.getMaxX(ptr);
		height = NCurses.getMaxY(ptr);
	}

	/** Destroy this window. Needs to be called when this
	 *  window will not be used anymore to remove NCurses object.
	 */
	public void close() {
		NCurses.delWin(windowPtr);
	}

	public int getX()		{ return x; }
	public int getY()		{ return y; }
	public int getWidth()	{ return width; }
	public int getHeight()	{ return height; }

	/** Application is blocked until User inputs a button.
	 *  A raw ascii value is returned for the pressed Button. */
	public char readChar() { return NCurses.getch(windowPtr); }
}

class JTUIWriter extends Writer {
	long ptr;

	public JTUIWriter(long p) { ptr = p; }

	public void close() {}
	public void flush() { NCurses.refresh(ptr); }
	public void write(char[] cbuf, int off, int len) { 
		NCurses.printw(ptr, new String(cbuf, off, len));
	}
}
