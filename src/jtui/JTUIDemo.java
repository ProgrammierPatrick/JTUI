package jtui;

public class JTUIDemo {
	public static void main(String[] args) {
		JTUI.start();

		final int size = 5;

		JTUIWindow root = JTUI.getRootWindow();
		JTUIWindow sideBar = new JTUIWindow(0, 0, size, root.getHeight());

		JTUIWindow main = new JTUIWindow(size, 0, root.getWidth()-size, root.getHeight());

		for(int i=0;i<root.getHeight();++i) {
			sideBar.out.println(i+":");
		}

		main.out.print("¹²³¼½¬{[]}¨þø→↓←ŧ¶€ł@ſðđŋħł…·µ”“„¢«»@æ»łſ«€ð¢¶đ„ŧŋ“");

		main.readChar();

		sideBar.close();
		main.close();
		JTUI.end();
	}
}
