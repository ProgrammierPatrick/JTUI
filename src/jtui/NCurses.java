package jtui;

import java.io.IOException;
import cz.adamh.utils.NativeUtils;

/** helper class for JNI-integration of ncurses */
class NCurses {

	private static boolean isRunning = false;

	static {
		try {
			//load from current Folder (for testing)
			System.loadLibrary("jtui");
		} catch(UnsatisfiedLinkError ee) {
			//load from jar (for distribution)
			try {
				NativeUtils.loadLibraryFromJar("natives/libjtui.so");
			} catch(IOException e) { throw new RuntimeException(e); }
		}
	}

	native private static long jtui_init();
	native private static void jtui_close();

	native private static void jtui_wprintw(long ptr, String str);
	native private static void jtui_wrefresh(long ptr);

	native private static long jtui_newwin(int h, int w, int y, int x);
	native private static void jtui_delwin(long ptr);

	native private static int  jtui_getmaxx(long ptr);
	native private static int  jtui_getmaxy(long ptr);
	
	native private static void jtui_wmove(long ptr, int y, int x);

	native private static char jtui_getch(long ptr);

	static long init() {
		if(isRunning) throw new RuntimeException("Could not init JTUI because it is already running.");
		long ptr = jtui_init();
		isRunning = true;
		return ptr;
	}
	static void end() {
		if(!isRunning) throw new RuntimeException("Could not end JTUI because it is not running.");
		jtui_close();
		isRunning = false;
	}

	static void refresh(long ptr) { jtui_wrefresh(ptr); }

	static void printw(long ptr, String str) {
		jtui_wprintw(ptr, str);
		jtui_wrefresh(ptr);
	}

	static long newWin(int x, int y, int w, int h) {
		long ptr = jtui_newwin(h, w, y, x);
		jtui_wrefresh(ptr);
		return ptr;
	}

	static void delWin(long ptr) { jtui_delwin(ptr); }

	static int getMaxX(long ptr) { return jtui_getmaxx(ptr); }
	static int getMaxY(long ptr) { return jtui_getmaxy(ptr); }

	static void move(long ptr, int x, int y) { jtui_wmove(ptr, y, x); }

	static char getch(long ptr) { return jtui_getch(ptr); }
	
}
